# Build container
FROM docker:20.10.6-dind AS builder

ENV DOCKER_COMPOSE_VERSION=1.29.1
ENV PYTHON3_VERSION=3.8.8-r0
ENV PIP3_VERSION=20.3.4-r0

ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1

# Install python and pip
RUN apk add --no-cache \
    python3=$PYTHON3_VERSION \
    py3-pip=$PIP3_VERSION

# Install docker-compose dependencies
RUN apk add --no-cache \
    python3-dev=$PYTHON3_VERSION \
    libffi-dev=3.3-r2 \
    openssl-dev=1.1.1k-r0 \
    gcc=10.2.1_pre1-r3 \
    libc-dev=0.7.2-r3 \
    make=4.3-r0

# Build and install docker-compose
RUN pip3 install --no-cache-dir docker-compose==$DOCKER_COMPOSE_VERSION

# Composer container
FROM docker:20.10.6-dind

LABEL maintainer="gabrielarellano@gmail.com"

ENV PYTHON3_VERSION=3.8.8-r0

# Add python
RUN apk add --no-cache python3=$PYTHON3_VERSION

# Copy docker-compose from build container
COPY --from=builder /usr/bin/docker-compose /usr/bin/docker-compose

